create database cEscolar;
use cEscolar;
create table usuario(
idusuario int primary KEY AUTO_INCREMENT,
nombre varchar(50),
apellidopaterno varchar(50),
apellidomaterno  varchar(50),
contrasena varchar(520)
);

drop table alumno;
create table alumno(
idalum int primary key auto_increment,
numcontrol int,
nombrealum varchar(50),
apellidopalum varchar(50),
apellidomalum varchar(50),
fechanacimiento varchar(50),
domicilio varchar(100),
correoelectronico varchar(50),
sexo varchar(15),
fkestado int,
fkciudad int,
foreign key(fkestado)references estados(idestado),
foreign key(fkciudad)references municipios(idmunicipio));

select * from alumno;
insert into alumno values(21321,'dsfs','sdf','dsf','ewrewr','werwer','ewr','wer');

create table estados(
idestado int primary key,
codigo varchar(5),
nombreest varchar(20));



create table municipios(
idmunicipio int primary key auto_increment,
nombreciudad varchar(30),
codigomunicip varchar(10),
fkidestado int,
foreign key (fkidestado) references estados (idestado));

drop table maestros;
create table maestros(
idmaestro int primary key auto_increment,
numcontrol_maestro varchar(10),
nombremaestro varchar(50),
apellidopatm varchar(50),
apellidomatm varchar(50),
direccionprof varchar(150),
fkestadomae int,
fkciudadmae int,
numcedula int,
titulo varchar(50),
fechanacimientomae date,
foreign key(fkestadomae) references estados(idestado),
foreign key(fkciudadmae) references municipios(idmunicipio)
); 

drop table estudios;
create table estudios(
idestudio int primary key auto_increment,
nombreposgrado varchar(150),
documento varchar(200),
fkmaestro int,
foreign key(fkmaestro) references maestros(idmaestro));

drop table materias;
create table materias(
idmateria varchar(20) primary key,
nombremateria varchar(100),
horasteoria int,
horaspractica int,
creditos int,
semestre varchar(30),
relacionantes varchar(20),
relacionpost varchar(20),
foreign key (relacionantes) references materias(idmateria),
foreign key (relacionpost) references materias(idmateria));

insert into materias values('fis','funsoft',4,4,8,'quinto',null,null);
insert into materias values('tbd','tallerbasedatos',3,3,6,'quinto','fis',null);
insert into materias values('red','tallerbasedatos',3,3,6,'quinto','fis','tbd');
insert into materias values('dor','funsoft',4,4,8,'quinto',null,'fis');




select * from materias;

create table escuela(
controlescuela varchar(10),
nombre_escuela varchar(200),
rfc varchar(50),
domicilioescuela varchar(200),
telefonoescuela varchar(12),
correoescuela varchar(100),
paginawebescuela varchar(100),
director varchar(200),
logoescuela varchar(200));

drop table grupos;
create table grupos(
controlgrupo int primary key AUTO_INCREMENT,
nombregrupo varchar(100)
);

create TABLE asignargrupo (
idasignargrupo int primary key auto_increment,
fkgrupo int,
alumno int,
foreign key(fkgrupo) references grupos(controlgrupo),
foreign key(alumno) references alumno(idalum));

drop table asignacion;
create TABLE asignacion(
idasignacion int primary KEY AUTO_INCREMENT,
maestro int,
grupo int,
materia varchar(20),
foreign KEY(maestro) REFERENCES maestros(idmaestro),
foreign KEY(grupo) REFERENCES grupos(controlgrupo),
foreign KEY(materia) REFERENCES materias(idmateria));

drop table calificaciones;
create TABLE Calificaciones(
idcalificacion int primary key AUTO_INCREMENT,
primer_parcial double,
segundo_parcial double,
tercer_parcial double,
cuarto_parcial double,
fkalumno int,
fkgrupo int,
materia varchar(20),
foreign key(fkalumno) references alumno(idalum),
foreign key(fkgrupo) references grupos(controlgrupo),
foreign key(materia) references materias(idmateria));
select * from alumno;
insert into calificaciones values(1,98,98,98,98,5,1,'tbd');
drop view v_grupo;
create view v_grupo as
select idasignargrupo as "ID", controlgrupo as "IDgrupo",nombregrupo as "Nombre_Grupo",idalum as "id_Alumno",nombrealum as "Nombre_Alumno",apellidopalum as "ApellidoPaterno_Alumno",apellidomalum as "ApellidoMaterno_Alumno" from alumno,asignargrupo,grupos where asignargrupo.alumno = alumno.idalum and asignargrupo.fkgrupo = grupos.controlgrupo;
select * from v_grupo;
create view v_asignacion as
select idasignacion as "ID",nombregrupo as "Grupo",numcontrol_maestro as "Numero_control",nombremaestro as "Maestro",apellidopatm as "ApellidoPaterno",apellidomatm as "ApellidoMaterno",nombremateria as "Materia",creditos as "Creditos" from asignacion,materias,grupos,maestros where maestro = idmaestro and grupo = controlgrupo and materia = idmateria;
select * from asignacion;
select * from v_asignacion;

drop view v_calificacion;
create view v_calificacion as
select idcalificacion as "ID",controlgrupo as "Control_grupo",nombregrupo as "Grupo",idmateria,nombremateria as "Nombre_Materia",idalum as "Id_Alumno",nombrealum as "Alumno",primer_parcial,segundo_parcial,tercer_parcial,cuarto_parcial from calificaciones,alumno,grupos,materias where fkgrupo = controlgrupo and fkalumno = idalum and idmateria = materia;
select * from v_calificacion;